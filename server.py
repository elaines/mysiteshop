from flask import Flask, render_template, request
from goods import Good
import goods_storage

app = Flask(__name__)

'''GOODS = [
    Good ('sport', 10, 'photos of sport events'),
    Good ('horses', 20, 'photos of horse'),
    Good ('ad', 30, 'ad photos')
]
'''

STORAGE = goods_storage.Goods_Storage('my_goods.db')
@app.route('/')
def hello_world():
    #return render_template('main.html', text='Hello, World!', goods = GOODS )
    return render_template('main.html', text='Hello, World!', goods=STORAGE.get_all_goods())

@app.route('/add/',methods=['post', 'get'] )
def add_order():
    if request.method == 'POST':
        print(request.form.get('user_name'))
        good = Good (name=request.form.get('user_name'),
                     price=request.form.get('goods_price'),
                     description=request.form.get('user_message'))
        #GOODS.append(good)
        STORAGE.add_goods(good)
    return render_template('add.html')

app.run()



