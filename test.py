import requests
from bs4 import BeautifulSoup

import unittest

class TestAnswer (unittest.TestCase):

  def test_response_site(self):
      response = requests.get('http://127.0.0.1:5000/')
      self.assertEqual(response.status_code, 200)

      soup = BeautifulSoup(response.text, 'html.parser')

      self.assertEqual(soup.body.h1.get_text(), 'Hello, World!')

      goods = soup.find_all('li')

      goods = [g.get_text() for g in goods]
      #self.assertListEqual(goods, ['Sport', 'Horses', 'Ad'])
      self.assertListEqual(goods, ['sport 10 photos of sport events', 'horses 20 photos of horse', 'ad 30 ad photos'])

if __name__ == '__main__':
    unittest.main()

