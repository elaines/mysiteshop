import sqlite3
from goods import Good
class Goods_Storage:
    def __init__(self, file_name):
        self.conn = sqlite3.connect(file_name, check_same_thread=False)
        c = self.conn.cursor()

        c.execute ('''CREATE TABLE IF NOT EXISTS goods
             (good_name text, price text, description text)''')

    def add_goods (self, good):
        c = self.conn.cursor()
        c.execute("INSERT INTO goods (good_name, price, description) VALUES (?,?,?)", (good.name, good.price, good.description))

    def get_all_goods(self):
        c = self.conn.cursor()
        goods = []
        for row in c.execute('SELECT good_name, price, description FROM goods'):
            goods.append(Good(name = row[0], price=row[1], description=row[2]))

        return goods